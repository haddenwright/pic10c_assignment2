* This is a program to calculate final course grades.
* This is the second assignment for the class at ucla titled PIC 10 C. 
* The intent of this assignment was to become familiar with using the QT framework.
* This program has a user designed window and custom connected widgets for easy grade calculation.
* Functionalities inculded are: automatically selecting the optimal grading schema as laid out in the syllabus,
* Functionalities yet to be inculded are: changing the class from 10C to 10B and vice versa, changing indivdual homework/exam scores and seeing how that affects the final grade instantaneously.
* Things worth noting: 
	It is intented that in QT 5 new syntax allows for lambda's, binders, and even normal functions. However I tried using this to my advantage; however, the Qt compiler did not like what I was doing and thereby did not build/run. 
	When using a custom Qt class, the compiler would not like how I declared the ui member field and thereby would not compile/run.
	This program does compile; however, it does not display any of the desired widgets. Despite my best efforts and following, as I understand them, the conventions laid out on Professor Salazar's notes, I have not been able to get this program to work as intended. 
	
