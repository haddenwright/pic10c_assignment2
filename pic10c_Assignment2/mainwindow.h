#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSpinbox>
#include <QSlider>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <vector>
#include <QLabel>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void update_grade(int);

private:
    Ui::MainWindow *ui;
    std::vector<QSpinBox*> homeworkBoxes;
    std::vector<QSpinBox*> examBoxes;
    std::vector<QSlider*> homeworkSlides;
    std::vector<QSlider*> examSlides;
    QVBoxLayout* homeworkLayout;
    QVBoxLayout* examLayout;
    QHBoxLayout* overall;
    std::vector<QLabel*> homeworkLabels;
    std::vector<QLabel*> examLabels;
    QLabel* FinalGrade;
    QLabel* Title;
};

#endif // MAINWINDOW_H
