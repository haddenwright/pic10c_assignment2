#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //populate and connect homework boxes and sliders
    for (size_t i = 0 ; i < 8 ; i++){
        homeworkBoxes.push_back(new QSpinBox());
        homeworkBoxes[i] ->setRange(0,100);
        homeworkSlides.push_back(new QSlider());
        homeworkSlides[i] ->setRange(0,100);
        QObject::connect(homeworkBoxes[i],SIGNAL(valueChanged(int)),homeworkSlides[i], SLOT(setValue(int)));
        QObject::connect(homeworkSlides[i],SIGNAL(valueChanged(int)),homeworkBoxes[i], SLOT(setValue(int)));
        QObject::connect(homeworkBoxes[i] , SIGNAL(valueChanged(int)), this, SLOT(update_grade(int)));
    }
    //populate and connect exam boxes and sliders
    for (size_t i = 0 ; i < 3 ; i++){
        examBoxes.push_back( new QSpinBox());
        examBoxes[i]->setRange(0,100);
        examSlides.push_back( new QSlider());
        (examSlides[i])->setRange(0,100);
        QObject::connect(examBoxes[i],SIGNAL(valueChanged(int)),examSlides[i], SLOT(setValue(int)));
        QObject::connect(examSlides[i],SIGNAL(valueChanged(int)),examBoxes[i], SLOT(setValue(int)));
        QObject::connect(examBoxes[i] , SIGNAL(valueChanged(int)), this, SLOT(update_grade(int)));
    }

    //create homework labels
    for(size_t i = 0 ; i < 9 ; i++){
        homeworkLabels.push_back(new QLabel());
    }
    //populate homework labels
    homeworkLabels[0] -> setText("Homework");
    for (size_t i = 1 ; i < 9 ; i++){
       homeworkLabels[i] -> setText(QString::number(i));
    }
    //create exam labels
    for(size_t i = 0 ; i < 4 ; i++){
        examLabels.push_back( new QLabel());
    }
    //populate exam labels
    examLabels[0] -> setText("Exams");
    for (size_t i = 1 ; i < 4 ; i++){
      examLabels[i] -> setText(QString::number(i));
    }

    FinalGrade = new QLabel();
    Title = new QLabel();
   //set up homework layout
   homeworkLayout = new QVBoxLayout();
   homeworkLayout->addWidget(homeworkLabels[0]);
   for (size_t i = 0 ; i < 8 ; i++){
       QHBoxLayout* temp = new QHBoxLayout();
       temp->addWidget(homeworkLabels[i+1]);
       temp->addWidget(homeworkBoxes[i]);
       temp->addWidget(homeworkSlides[i]);
       homeworkLayout ->addLayout(temp);
   }

   //set up exam layout
   examLayout = new QVBoxLayout();
   examLayout->addWidget(examLabels[0]);
   for (size_t i = 0 ; i < 3 ; i++){
       QHBoxLayout* temp = new QHBoxLayout();
       temp->addWidget(examLabels[i+1]);
       temp->addWidget(examBoxes[i]);
       temp->addWidget(examSlides[i]);
       examLayout ->addLayout(temp);
   }
   examLayout ->addWidget(FinalGrade);
   //set up final layout
   Title->setText("PIC 10 B Grade Calculator");
   overall = new QHBoxLayout();
   overall -> addWidget(Title);
   overall ->addLayout(homeworkLayout);
   overall -> addLayout(examLayout);

   //this->setLayout(overall);

}

void MainWindow::update_grade(int unused){
    double totalScheme1 = 0;
    double totalScheme2 = 0;
    //identify lowest 2 homeworks
    size_t indexDropOne = 0;
    size_t indexDropTwo = 1;
    for (size_t i = 2 ; i < 8; i++){
        if((homeworkBoxes[i]->value()) < (homeworkBoxes[indexDropOne]->value())){
            indexDropOne = i;
        }
        else if((homeworkBoxes[i]->value()) < (homeworkBoxes[indexDropTwo]->value())){
            indexDropTwo = i;
        }
    }
    // calculate homework portion of grade
    for (size_t i = 0 ; i < 8; i++ ){
        if( (i != indexDropOne) || (i =! indexDropTwo)){
                totalScheme1 += (homeworkBoxes[i]->value()) * .25 / (6);
                totalScheme2 = totalScheme1;
         }
    }
    //calculate midterm portion of grade
    for (size_t i = 0 ; i < 2 ; i++ ){
        totalScheme1 += (examBoxes[i]->value()) * .2 / (2);
    }
    {
    size_t highest = 0;
    for (size_t i = 2 ; i < 8; i++){
        if(homeworkBoxes[i]->value() > homeworkBoxes[highest]->value()){
            highest = i;
         }
     }
     totalScheme2 += examBoxes[highest]->value() *.3;
     }

    //calcuate exam portion
    totalScheme1 += examBoxes[2]->value() * .35;
    totalScheme2 += examBoxes[2]->value() * .44;

   //display percentage
   if (totalScheme1 > totalScheme2){
        FinalGrade->setText(QString::number(totalScheme1));

    }else{
         FinalGrade->setText(QString::number(totalScheme2));
   }
}
MainWindow::~MainWindow()
{
    delete ui;
}
